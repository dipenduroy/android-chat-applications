package tools;

import java.security.KeyFactory;
import java.security.PublicKey;
import java.security.spec.KeySpec;
import java.security.spec.X509EncodedKeySpec;

import securitymanager.KeyManager;

public class Util 
{
	public static String encode(byte [] b)
	{
		StringBuffer encoded = new StringBuffer();
		encoded.append(b[0]);	
		for(int i = 1; i < b.length; i++ )
			encoded.append(";" + b[i]);
		
		return encoded.toString();
	}
	
	public static byte[] decode(String str)
	{
		String[] myBack = str.split(";");
		byte[] decodedBack = new byte[myBack.length];
		
		for(int j = 0; j < myBack.length; j++)
		{
			byte temp = new Byte(myBack[j]);
			decodedBack[j] = temp;
		}
		return decodedBack;
	}
	
}
