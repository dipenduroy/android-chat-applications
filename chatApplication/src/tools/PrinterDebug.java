package tools;

import eu.sapere.android.SapereConsoleActivity;
import android.app.IntentService;
import android.content.Context;
import android.content.Intent;

public class PrinterDebug
{
	public static Context c;
    public static void printConsole(String mex)
	{
		Intent broadcastIntent = new Intent();
        broadcastIntent.setAction(SapereConsoleActivity.ConsoleDebugReceiver.ACTION_RESP);
        broadcastIntent.addCategory(Intent.CATEGORY_DEFAULT);
        broadcastIntent.putExtra(SapereConsoleActivity.ConsoleDebugReceiver.MEX_DEBUG, mex);
        c.getApplicationContext().sendBroadcast(broadcastIntent);
	}
    
    public static void printLog(String mex, String color)
	{
		Intent broadcastIntent = new Intent();
        broadcastIntent.setAction(SapereConsoleActivity.ConsoleLogReceiver.ACTION_RESP);
        broadcastIntent.addCategory(Intent.CATEGORY_DEFAULT);
        broadcastIntent.putExtra(SapereConsoleActivity.ConsoleLogReceiver.MEX_DEBUG, mex);
        broadcastIntent.putExtra(SapereConsoleActivity.ConsoleLogReceiver.MEX_COLOR, color);
        c.getApplicationContext().sendBroadcast(broadcastIntent);
	}
    
    public static void printDebugLog(String mex, String color)
   	{
   		Intent broadcastIntent = new Intent();
       broadcastIntent.setAction(SapereConsoleActivity.ConsoleDebugMessagesReceiver.ACTION_RESP);
       broadcastIntent.addCategory(Intent.CATEGORY_DEFAULT);
       broadcastIntent.putExtra(SapereConsoleActivity.ConsoleDebugMessagesReceiver.MEX_DEBUG, mex);
       broadcastIntent.putExtra(SapereConsoleActivity.ConsoleDebugMessagesReceiver.MEX_COLOR, color);
       c.getApplicationContext().sendBroadcast(broadcastIntent);
   	}
    
    public static void setStatusColor(String file)
    {
    		Intent broadcastIntent = new Intent();
        broadcastIntent.setAction(SapereConsoleActivity.ConsoleColorReceiver.ACTION_RESP);
        broadcastIntent.addCategory(Intent.CATEGORY_DEFAULT);
        broadcastIntent.putExtra(SapereConsoleActivity.ConsoleColorReceiver.MEX_DEBUG, file);
        c.getApplicationContext().sendBroadcast(broadcastIntent);
    }
    
    public static void setStatusId(String id)
    {
    		Intent broadcastIntent = new Intent();
        broadcastIntent.setAction(SapereConsoleActivity.ConsoleIdReceiver.ACTION_RESP);
        broadcastIntent.addCategory(Intent.CATEGORY_DEFAULT);
        broadcastIntent.putExtra(SapereConsoleActivity.ConsoleIdReceiver.MEX_DEBUG, id);
        c.getApplicationContext().sendBroadcast(broadcastIntent);
    }
    
    public static void setGradientId(String id)
    {
    		Intent broadcastIntent = new Intent();
        broadcastIntent.setAction(SapereConsoleActivity.ConsoleGradientReceiver.ACTION_RESP);
        broadcastIntent.addCategory(Intent.CATEGORY_DEFAULT);
        broadcastIntent.putExtra(SapereConsoleActivity.ConsoleGradientReceiver.MEX_DEBUG, id);
        c.getApplicationContext().sendBroadcast(broadcastIntent);
    }
    
    public static void disableButton()
    {
    		Intent broadcastIntent = new Intent();
        broadcastIntent.setAction(SapereConsoleActivity.ConsoleDisableButton.ACTION_RESP);
        broadcastIntent.addCategory(Intent.CATEGORY_DEFAULT);
        c.getApplicationContext().sendBroadcast(broadcastIntent);
    }
    
}
