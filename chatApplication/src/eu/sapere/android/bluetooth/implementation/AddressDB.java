package eu.sapere.android.bluetooth.implementation;

import java.util.Iterator;
import java.util.concurrent.ConcurrentHashMap;

import android.app.Application;
import android.util.Log;
 
/**
 * The AddressDB class provides a static and synchronized ConcurrentHashMap
 * containing the references to BT devices being paired and connected.
 * <p>
 * Each entry is a couple < String, Record > where String represents the device
 * BT MacAddress while Record is an object containing device WIFI IP, the
 * distance expressed in RSSI dB, the neighbourName, the neighbourType and the
 * last seen time (expressed in millis from 01/01/1970).
 * <p>
 * AddressDB extends Application, in Android this makes the instance visible in
 * all classes.
 * 
 * @author Alberto Rosi
 */

public class AddressDB extends Application {

	private static final String TAG = "App_Address Book";
	private static final long TTL = 1000 * 40; // Remove hosts if not seen after
	// 40 seconds

	static ConcurrentHashMap<String, Record> records;

	String mac_address;
	String ip;
	int rssi;

	/**
	 * The constructor initializes a new AddressDB, or rather, a new
	 * ConcurrentHashMap storing the AddressDB.
	 */

	public AddressDB() {
		records = new ConcurrentHashMap<String, Record>();

	}

	/**
	 * Retrieves a reference to actual AddressDB instance. Before delivering the
	 * instance, a clean is performed comparing AddressDB TTL and devices last
	 * seen time.
	 * 
	 * @return the {@link ConcurrentHashMap} storing the AddressDB
	 */

	public ConcurrentHashMap<String, Record> getRecords() {
		cleanAddressBook();
		return records;
	}

	/**
	 * Removes a device from AddressDB if not seen after TTL.
	 */

	private synchronized void cleanAddressBook() {
		Iterator<String> records_iterator = records.keySet().iterator();

		while (records_iterator.hasNext()) {
			String key = records_iterator.next();
			Record value = records.get(key);
			long actual_time = System.currentTimeMillis();
			if (value.time_in_millis + TTL < actual_time) {
				records.remove(key);
				Log.d(TAG, "Host removed: " + key);
			}
		}
	}

	/**
	 * Adds the found device to AddressAB, if the device is already known its
	 * information is updated. Device Mac Address if not in xx:xx:xx format is
	 * formatted by formatMacAddress(String) method.
	 */

	private synchronized void createOrUpdate(String device_mac_address,
			String ip, int rssi, String neighbourName, String neighbourType) {
		device_mac_address = formatMacAddress(device_mac_address);
		Record rec = records.get(device_mac_address);
		Log.d(TAG, "createOrUpdate: " + device_mac_address);

		if (rec == null) {
			Log.d(TAG, "NewAddress: " + device_mac_address);
			// Creates entry only for Sapere device, namely device that
			// communicates the IP
			Record newRec = new Record(ip, rssi, neighbourName, neighbourType);
			rec = records.putIfAbsent(device_mac_address, newRec);
		}

		else {
			Log.d(TAG, "OldAddress: " + device_mac_address);
			if (ip == null) {
				Log.d(TAG, "update rssi: " + rssi);
				rec.setRssi(rssi);
				// Log.d(TAG,"at time:"+rec.getUpdateTime());
			}
			if (ip != null) {
				Log.d(TAG, "update ip: " + ip);
				rec.setIp(ip);
			}
		}
	}

	/**
	 * Adds found device to AddressDB. Params requested are its macaddress, ip,
	 * neighbourName and neighbourType.
	 * 
	 * @param mac_address
	 *            device mac address
	 * @param ip
	 *            device wifi ip
	 * @param neighbourName
	 *            device neighbourName
	 * @param neighbourType
	 *            device neighbourType
	 */
	public void processIp(String mac_address, String ip, String neighbourName,
			String neighbourType) {
		createOrUpdate(mac_address, ip, 0, neighbourName, neighbourType);
	}

	/**
	 * For a given device (identified by its BT mac address) this method updates
	 * its distance in terms of RSSI dB.
	 * 
	 * @param mac_address
	 * @param rssi
	 */
	public void processRssi(String mac_address, int rssi) {
		createOrUpdate(mac_address, null, rssi, null, null);
	}

	/**
	 * Returns the AddressDB size
	 * 
	 * @return the {@link Integer} representing AddressDB size
	 */
	public int getLength() {
		return records.size();
	}

	/**
	 * Separates mac address couples using ":"
	 * 
	 * @return the {@link String} reformatted mac address
	 */

	private String formatMacAddress(String s) {
		String result = "";
		int i = 0;
		int string_length = s.length();
		if (string_length < 13) {
			while (i < string_length) {
				result += s.charAt(i++);
				result += s.charAt(i++);
				result += ":";

			}
			result = result.substring(0, result.length() - 1);
		} else {
			result = s;
		}
		result = result.toUpperCase();
		return result;
	}

	/**
	 * Record is an object containing device WIFI IP, the distance expressed in
	 * RSSI dB, the neighbourName, the neighbourType and the "last seen" time
	 * (expressed in millis from 01/01/1970). Each time the IP or the RSSI dB
	 * field in Record is updated, "last seen" time gets updated.
	 * 
	 * @author Alberto Rosi
	 */

	public class Record {

		private String ip;

		private int rssi;

		private String neighbourName;

		private String neighbourType;

		private long time_in_millis = 0;

		/**
		 * It instantiates a new Record.
		 * 
		 * @param ip
		 *            device WIFI IP
		 * @param rssi
		 *            distance in RSSI dB
		 * @param neighbourName
		 *            the neighbour Name
		 * @param neighbourType
		 *            the neighbour Type
		 */

		public Record(String ip, int rssi, String neighbourName,
				String neighbourType) {
			this.ip = ip;
			this.rssi = rssi;
			this.neighbourName = neighbourName;
			this.neighbourType = neighbourType;
			setUpdateTime();
		}

		/**
		 * Returns device WIFI IP.
		 * 
		 * @return the {@link String} representing device WIFI IP
		 */
		public String getIp() {
			return ip;
		}

		/**
		 * Sets device WIFI IP.
		 * 
		 * @param ip
		 *            device WIFI IP
		 */
		public void setIp(String ip) {
			this.ip = ip;
			setUpdateTime();
		}

		/**
		 * Returns device distance in RSSI dB.
		 * 
		 * @return the {@link Integer} representing device RSSI dB
		 */
		public int getRssi() {
			return rssi;
		}

		/**
		 * Sets device RSSI dB distance.
		 * 
		 * @param rssi
		 *            distance in dB
		 */

		public void setRssi(int rssi) {
			this.rssi = rssi;
			setUpdateTime();
		}

		/**
		 * Returns device "last seen" time in millis from 01/01/1970.
		 * 
		 * @return the {@link Long} represeting time in millis from 01/01/1970
		 */

		public long getUpdateTime() {
			return time_in_millis;
		}

		/**
		 * Sets device "last seen" to current Time.
		 */

		private void setUpdateTime() {
			time_in_millis = System.currentTimeMillis();
		}

		/**
		 * Returns the Neighbour Name.
		 * 
		 * @return the {@link String} representing Neighbour Name
		 */
		public String getNeighbourName() {
			return neighbourName;
		}

		/**
		 * Sets the Neighbour Name.
		 * 
		 * @param neighbourName
		 *            the string representing Neighbour Name
		 */
		public void setNeighbourName(String neighbourName) {
			this.neighbourName = neighbourName;
		}

		/**
		 * Returns the Neighbour Type.
		 * 
		 * @return the {@link String} representing Neighbour Type
		 */
		public String getNeighbourType() {
			return neighbourType;
		}

		/**
		 * Sets the Neighbour Type.
		 * 
		 * @param neighbourType
		 *            the string representing Neighbour Type
		 */
		public void setNeighbourType(String neighbourType) {
			this.neighbourType = neighbourType;
		}
	}
}
