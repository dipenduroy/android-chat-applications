package eu.sapere.android.bluetooth.implementation;

import eu.sapere.android.bluetooth.implementation.BtManager.BTResponseReceiver;
import android.app.IntentService;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothSocket;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.util.Log;

/**
 * The BtRadarService is an IntentService measuring visible Bluetooth devices
 * RSSI. RSSI is the relative received signal strength in a wireless
 * environment, in arbitrary units. RSSI is an indication of the power level
 * being received by the antenna. Therefore, the higher the RSSI number (or less
 * negative in some devices), the stronger the signal, the nearest is the
 * device.
 * 
 * @author Alberto Rosi
 * 
 */
public class BtRadarService extends IntentService {

	private static final String TAG = "App_BtRadarService";
	Object monitor = new Object();
	BluetoothSocket socket = null;
	static Intent broadcastIntent = new Intent();
	private static RadarReceiver receiver;
	IntentFilter filter;
	static BtRadarService myinstance = null;

	/**
	 * Basic empty IntentService constructor calling onCreate() routine.
	 */
	public BtRadarService() {
		super("BtRadarService");

	}

	/**
	 * Registers for Bluetooth Adapter discovery events.
	 */
	public void onCreate() {
		super.onCreate();

		myinstance = this;

		filter = new IntentFilter();
		filter.addAction(BluetoothDevice.ACTION_FOUND);
		filter.addAction(BluetoothAdapter.ACTION_DISCOVERY_STARTED);
		filter.addAction(BluetoothAdapter.ACTION_DISCOVERY_FINISHED);
		filter.addCategory(Intent.CATEGORY_DEFAULT);
		receiver = new RadarReceiver();
		registerReceiver(receiver, filter);
	}

	/**
	 * On startService() the RadarService commits {@link BluetoothAdapter} to
	 * start a device discovery internal routine. Once discovery has finished,
	 * the couple ( Device Mac Address, RSSI value ) is processed by the
	 * AddressDB.
	 */

	protected void onHandleIntent(Intent intent) {

		BluetoothAdapter mBluetoothAdapter = BluetoothAdapter
				.getDefaultAdapter();

		mBluetoothAdapter.startDiscovery();

		synchronized (monitor) {
			try {
				monitor.wait();
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				Log.e(TAG, e.getMessage());
			}
		}

		notifyEnd();
	}

	private static BtRadarService getInstance() {
		return myinstance;
	}

	/**
	 * Closes active BT discovery.
	 */
	public void onDestroy() {

		unregisterReceiver(receiver);

	}

	/**
	 * Notify {@link BtManager} that the current instance of RadarService has
	 * terminated.
	 */
	public static void notifyEnd() {

		broadcastIntent.setAction(BTResponseReceiver.FINISH_RESP_RADAR);
		broadcastIntent.addCategory(Intent.CATEGORY_DEFAULT);
		getInstance().sendBroadcast(broadcastIntent);
	}

	/**
	 * The RadarReceiver class manages the three possible states of service.
	 * These are:
	 * <p>
	 * <li>Discovery started: no action taken, waiting for device being found.
	 * <li>Device found: the couple ( Device Mac Address, RSSI value ) is
	 * processed by the {@link AddressDB}.
	 * <li>Discovery finished: the event is notified to {@link BtManager}.
	 * 
	 * @author Alberto Rosi
	 * 
	 */
	public class RadarReceiver extends BroadcastReceiver {

		AddressDB ab = ((AddressDB) getApplicationContext());

		/**
		 * Receives events from {@link BluetoothAdapter} and takes decision.
		 */
		public void onReceive(Context context, Intent intent) {
			/**
			 * A device is found.
			 */
			if (intent.getAction().equals(BluetoothDevice.ACTION_FOUND)) {
				populateRSSIvalues(intent);
			}

			/**
			 * A new discovery routine has started.
			 */

			if (intent.getAction().equals(
					BluetoothAdapter.ACTION_DISCOVERY_STARTED)) {
				Log.d(TAG, "Radar Started!");
			}

			/**
			 * A discovery routine has finished.
			 */

			if (intent.getAction().equals(
					BluetoothAdapter.ACTION_DISCOVERY_FINISHED)) {
				synchronized (monitor) {
					Log.d(TAG, "Radar Stopped!");
					monitor.notifyAll();
				}
			}
		}

		/**
		 * For each discovered device the couple ( Device Mac Address, RSSI
		 * value ) is sent to the {@link AddressDB}.
		 */
		private void populateRSSIvalues(Intent intent) {

			BluetoothDevice device = intent
					.getParcelableExtra(BluetoothDevice.EXTRA_DEVICE);
			int rssi = intent.getShortExtra(BluetoothDevice.EXTRA_RSSI,
					Short.MIN_VALUE);
			ab.processRssi(device.getAddress(), rssi);
			Log.d(TAG, device.getName() + " " + device.getAddress() + " RSSI: "
					+ rssi + "dBm");
		}

	}

}
