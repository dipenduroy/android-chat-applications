package eu.sapere.android.bluetooth.implementation;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.net.InetAddress;
import java.net.NetworkInterface;
import java.net.SocketException;
import java.util.Enumeration;

import android.bluetooth.BluetoothSocket;
import android.util.Log;

/**
 * This class manages the opened client/server BT connection between Sapere
 * devices. The handshaking sees the exchange of 4 parameters: <li>WIFI IP <li>
 * BT MAC ADDRESS <li>Node Neighbour Name <li>Node Neighbour Type
 * <p>
 * Received information is used to populate AddressDB.
 * 
 * @author Alberto Rosi
 * 
 */
public class ServerConnEstab extends Thread {

	private static final String TAG = "App_Server_ConnEstablished";
	private BluetoothSocket socket = null;
	String data = null;
	Object monitor;
	String my_ip = null;
	DataInputStream in;
	DataOutputStream out;
	AddressDB ab;

	// inputs
	String ip = null;
	String mac_address = null;
	String neighbourName = null;
	String neighbourType = null;

	/**
	 * The ServerConnEstab constructor retrieves the parameters necessary for
	 * devices handshaking.
	 * 
	 * @param socket
	 *            the reference to client BT socket
	 * @param monitor
	 *            the reference to Object monitor
	 * @param ab
	 *            the reference to application AddressDB
	 * @param neighbourName
	 *            local device neighbourName
	 * @param neighbourType
	 *            local device neighbourType
	 */
	public ServerConnEstab(BluetoothSocket socket, Object monitor,
			AddressDB ab, String neighbourName, String neighbourType) {
		this.socket = socket;
		this.ab = ab;
		this.neighbourName = neighbourName;
		this.neighbourType = neighbourType;
		this.monitor = monitor;
		my_ip = getLocalIpAddress();

	}

	/**
	 * Realizes the handshaking between devices.
	 */
	public void run() {

		try {
			synchronized (monitor) {

				Log.d(TAG, "-->Connection Accepted: \n");

				in = new DataInputStream(socket.getInputStream());

				out = new DataOutputStream(socket.getOutputStream());

				out.writeUTF(my_ip);
				out.writeUTF(neighbourName);
				out.writeUTF(neighbourType);

				ip = in.readUTF();
				neighbourName = in.readUTF();
				neighbourType = in.readUTF();
				mac_address = in.readUTF();

				Log.d(TAG, "-->Network token recv: " + ip + " , " + mac_address
						+ " " + neighbourName + " " + neighbourType + " \n");

				ab.processIp(mac_address, ip, neighbourName, neighbourType);

				socket.close();
				Log.d(TAG, "-->Connection terminated");
				monitor.notifyAll();

			}

		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			try {
				socket.close();
			} catch (IOException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
			monitor.notifyAll();
		}

	}

	/**
	 * Retrieves device WIFI IP address.
	 * 
	 * @return device IP
	 */
	public static String getLocalIpAddress() {
		try {
			for (Enumeration<NetworkInterface> en = NetworkInterface
					.getNetworkInterfaces(); en.hasMoreElements();) {
				NetworkInterface intf = en.nextElement();
				for (Enumeration<InetAddress> enumIpAddr = intf
						.getInetAddresses(); enumIpAddr.hasMoreElements();) {
					InetAddress inetAddress = enumIpAddr.nextElement();
					if (!inetAddress.isLoopbackAddress()) {
						return inetAddress.getHostAddress();
					}
				}
			}
		} catch (SocketException ex) {
			return "Not connected";
		}
		return "Not connected";
	}

}
