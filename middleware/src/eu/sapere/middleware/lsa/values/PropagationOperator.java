package eu.sapere.middleware.lsa.values;

/**
 * Enumerates Propagation-specific properties
 * @author Gabriella Castelli (UNIMORE)
 *
 */
	
public enum PropagationOperator {
		
		DIFFUSION_OP("diffusion_op"),
		
		FIELD_VALUE("field_value"),
		
		SOURCE("souce"),
		
		PREVIOUS ("previous"),
		
		DESTINATION ("destination"),
		
		AGGREGATION_OP("aggregation_op"),
		
		DIRECT("direct");
		
		  private String text;

		  PropagationOperator (String text) {
		    this.text = text;
		  }

		  public String getText() {
		    return this.text;
		  }

		  public static PropagationOperator fromString (String text) {
		    if (text != null) {
		      for (PropagationOperator b : PropagationOperator.values()) {
		        if (text.equalsIgnoreCase(b.text)) {
		          return b;
		        }
		      }
		    }
		    return null;
		  }

		
		
		
		public String getOperator (String op){
			return op.substring(op.indexOf("_"));
			
		//	return aOp;
		}
		
		public int getHop(String op){
			return new Integer(op.substring(op.indexOf("_"))).intValue();
		}
		
		
	}


