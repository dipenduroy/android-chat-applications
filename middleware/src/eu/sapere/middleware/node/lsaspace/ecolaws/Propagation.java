package eu.sapere.middleware.node.lsaspace.ecolaws;

import java.util.LinkedList;
import java.util.UUID;

import com.sun.tools.javac.util.List;

import sun.management.jmxremote.ConnectorBootstrap.PropertyNames;
import eu.sapere.middleware.lsa.Lsa;
import eu.sapere.middleware.lsa.Property;
import eu.sapere.middleware.lsa.values.PropertyName;
import eu.sapere.middleware.lsa.values.SyntheticPropertyName;
import eu.sapere.middleware.node.lsaspace.OperationManager;
import eu.sapere.middleware.node.lsaspace.Space;
import eu.sapere.middleware.node.networking.transmission.delivery.INetworkDeliveryManager;
import eu.sapere.middleware.node.networking.transmission.receiver.NetworkReceiverManager;
import eu.sapere.middleware.node.notifier.Notifier;
import eu.sapere.middleware.node.notifier.event.PropagationEvent;

/**
 * The Propagation eco-law implementation.
 * 
 * @author Gabriella Castelli (UNIMORE)
 */
public class Propagation extends AbstractEcoLaw {
	/**
	 * Creates a new instance of the Propagation eco-law.
	 * 
	 * @param space
	 *            The space in which the eco-law executes.
	 * @param opManager
	 *            The OperationManager that manages operations in the space
	 * @param notifier
	 *            The Notifier that notifies agents whith events happening to
	 *            LSAs
	 * @param networkDeliveryManager
	 *            The interface for Network Delivery of LSAs
	 */
	public Propagation(Space space, OperationManager opManager,
			Notifier notifier, INetworkDeliveryManager networkDeliveryManager) {
		super(space, opManager, notifier, networkDeliveryManager);
	}

	/**
	 * {@inheritDoc}
	 */
	public void invoke() {
		for (final Lsa lsa : getLSAs()) {
			if (isGradientPropagation(lsa)) {
				doGradientPropagation(lsa);
			} else if (isDirectPropagation(lsa)) {
				doDirectPropagation(lsa);
			}
		}
	}

	/**
	 * Propagate an LSA using direct propagation. The network delivery manager
	 * checks to see if the host is valid so we do not need to do that here.
	 * 
	 * @param an_lsa
	 *            the LSA to propagate.
	 */
	private void doDirectPropagation(Lsa an_lsa) {

		final String destinationName = an_lsa
				.getSingleValue(PropertyName.DESTINATION.toString());
		for (Lsa lsa : getLSAs()) {
			if (lsa.isNeighbour()) {
				if ((destinationName.equals("all") == false)
						&& lsa.getSingleValue("neighbour").equals(
								destinationName)) {
					propagate(an_lsa, lsa);
				} else if (destinationName.equals("all")){// all
					propagate(an_lsa, lsa);
				} else {
					//nothing
					int j;
					j = 1;
					int h=3;
					h++;
				}

			}
		}

	}

	private void propagate(Lsa an_lsa, Lsa lsa) {
		getNetworkDeliveryManager()
				.doSpread(directPropagationCopy(an_lsa), lsa);
		
		an_lsa.removeProperty(PropertyName.DIFFUSION_OP.toString());

		// IF VISUAL INSPECTOR ACTIVE, represent the PROPAGATION
		if (space.hasVisualInspector())
			this.space.visualPropagate(an_lsa.getId());

		// event triggered for each spread
		PropagationEvent event = new PropagationEvent(an_lsa);
		publish(event);
	}

	/**
	 * Propagate an LSA using gradient propagation
	 * 
	 * @param an_lsa
	 *            the LSA to propagate.
	 */
	private void doGradientPropagation(final Lsa an_lsa) {
		for (final Lsa lsa : getLSAs()) {
			if (lsa.isNeighbour())
				getNetworkDeliveryManager().doSpread(gradientCopy(an_lsa), lsa);
		}
		//an_lsa.addProperty(new Property(PropertyName.DECAY.toString(),"1"));
		//String uuid = an_lsa.getProperty("uuid").getValue().get(0);
		//NetworkReceiverManager.getIgnoreList().put(UUID.fromString(uuid));
		// IF VISUAL INSPECTOR ACTIVE, represent the PROPAGATION
		if (space.hasVisualInspector())
			this.space.visualPropagate(an_lsa.getId());
		PropagationEvent event = new PropagationEvent(an_lsa);
		publish(event);
	}

	/**
	 * Creates a copy of an LSA to be propagated, removing the id, bonds and
	 * other properties.
	 * 
	 * @param an_lsa
	 *            the LSA to be copied.
	 * @param the_finalHop
	 *            true if this is the final hop for the LSA, false otherwise.
	 * @return the LSA copy.
	 */
	private Lsa gradientCopy(final Lsa an_lsa) {
		Lsa copy = an_lsa.getCopy();
		copy.removeSyntheticProperty(SyntheticPropertyName.BONDS);
		copy.removeSyntheticProperty(SyntheticPropertyName.CREATION_TIME);
		copy.removeSyntheticProperty(SyntheticPropertyName.CREATOR_ID);
		copy.removeSyntheticProperty(SyntheticPropertyName.LAST_MODIFIED);
		copy.removeSyntheticProperty(SyntheticPropertyName.LOCATION);
		copy.addProperty(new Property(PropertyName.PREVIOUS.toString(),
				getSpaceName()));
		copy.setId(null);
		final int nextHop = getNextHop(copy);
		if (nextHop >= 1) {
			copy.addProperty(new Property(PropertyName.DIFFUSION_OP.toString(),
					"GRADIENT_" + nextHop));
			// re-added by Gabriella
			copy.addProperty(new Property(an_lsa
					.getProperty(PropertyName.FIELD_VALUE.toString())
					.getValue().elementAt(0), "" + nextHop));

		} else {
			// diffuse the LSA, but the diffuse LSA won't be propagated further
			copy.removeProperty(PropertyName.DIFFUSION_OP.toString());
			copy.removeProperty(PropertyName.DESTINATION.toString());
		}
		return copy;
	}

	/**
	 * Gets the next hop count for a gradient LSA.
	 * 
	 * @param an_lsa
	 *            the LSA to extract the hop count from.
	 * @return the next hop count.
	 */
	private int getNextHop(final Lsa an_lsa) {
		String diffusionOp = an_lsa.getSingleValue(PropertyName.DIFFUSION_OP
				.toString());
		int currentHop = Integer.parseInt(diffusionOp.substring(diffusionOp
				.indexOf("_") + 1));
		return currentHop - 1;
	}

	/**
	 * Creates a copy of an LSA to be propagated directly, removing the id,
	 * bonds and other properties.
	 * 
	 * @param an_lsa
	 *            the LSA to be copied.
	 * @return the LSA copy.
	 */
	private Lsa directPropagationCopy(final Lsa an_lsa) {
		Lsa copy = an_lsa.getCopy();
		copy.removeSyntheticProperty(SyntheticPropertyName.BONDS);
		copy.removeSyntheticProperty(SyntheticPropertyName.CREATION_TIME);
		copy.removeSyntheticProperty(SyntheticPropertyName.CREATOR_ID);
		copy.removeSyntheticProperty(SyntheticPropertyName.LAST_MODIFIED);
		copy.removeSyntheticProperty(SyntheticPropertyName.LOCATION);
		copy.addProperty(new Property(PropertyName.PREVIOUS.toString(),
				getSpaceName()));
		copy.removeProperty(PropertyName.DESTINATION.toString());
		copy.removeProperty(PropertyName.DIFFUSION_OP.toString());
		copy.setId(null);
		copy.removeBonds();
		return copy;
	}

	/**
	 * Whether the input LSA has direct propagation properties set.
	 * 
	 * @param an_lsa
	 *            the LSA to check for direct propagation.
	 * @return true if the LSA conforms, false otherwise.
	 */
	private static boolean isDirectPropagation(Lsa lsa) {
		boolean ret = false;
		if (lsa.hasPropagatotionOp(PropertyName.DIFFUSION_OP.toString()))
			ret = lsa.getProperty(PropertyName.DIFFUSION_OP.toString())
					.getValue().elementAt(0).equals("direct");
		return ret;
	}

	/**
	 * Whether the input LSA has gradient propagation properties set.
	 * 
	 * @param an_lsa
	 *            the LSA to check for gradient propagation.
	 * @return true if the LSA conforms, false otherwise.
	 */
	private static boolean isGradientPropagation(Lsa lsa) {
		boolean ret = false;
		if (lsa.hasPropagatotionOp(PropertyName.DIFFUSION_OP.toString()))
			ret = lsa.getProperty(PropertyName.DIFFUSION_OP.toString())
					.getValue().elementAt(0).contains("GRADIENT");
		return ret;

		/*
		 * FRANCESCO:
		 * 
		 * Francesco: prima era cos� if (diffusionOp.isPresent()) { final String
		 * diffusionMode = diffusionOp.get().substring(0,
		 * diffusionOp.get().indexOf("_")); result =
		 * "GRADIENT".equals(diffusionMode); }
		 * 
		 * ma deve essere if (diffusionOp.isPresent()&&
		 * diffusionOp.get().indexOf("_") >0 ) { � � � � final String
		 * diffusionMode = diffusionOp.get().substring(0,
		 * diffusionOp.get().indexOf("_")); � � � � result =
		 * "GRADIENT".equals(diffusionMode); � � � }
		 */
	}


}
