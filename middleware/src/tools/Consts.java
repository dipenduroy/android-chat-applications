package tools;

public class Consts 
{
	public static final String PROP_FROM_NAME = "FromAgentName";
	public static final String PROP_FROM_IP = "FromAgentIP";
	public static final int	MAX_HOPS = 10;
	public static final String PROP_AGGREGATED_VALUE="x";
	public static final String CHEMO_GRADIENT = "chemoGradient";
	public static final String CHEMO_CHEMO = "chemoChemo";
	public static final String CONFIG_FILE="sdcard/sapere/config.txt";
	public static final String NODE_SENDER = "Tablet1";
	public static final String NODE_RECEIVER = "Tablet4";
	//public static final String NODE_BRIDGE = "Tablet5";
}
